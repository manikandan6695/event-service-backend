import { ParticipantService } from "./participant.service";
import {
  Body,
  Controller,
  Post,
  ValidationPipe,
  Res,
  Param,
  Get,
  Query,
  Patch,
} from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import {
  CreateParticipantDTO,
  participantsDTO,
} from "./dto/create-participant.dto";
import { Response } from "express";

@Controller("participant")
export class ParticipantController {
  constructor(
    private readonly participantService: ParticipantService,
    private sservice: SharedService
  ) {}

  @Post("add-participant")
  async addParticipant(
    @Body(new ValidationPipe()) body: participantsDTO,
    @Res() res: Response
  ) {
    try {
      let data = await this.participantService.addParticipant(
        body,
        body.created_by,
        body.updated_by
      );
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
  @Get("get-participants/:event_id")
  async getParticipants(
    @Param("event_id") event_id: string,
    @Res() res: Response
  ) {
    try {
      let data = await this.participantService.getParticipants(event_id);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @Patch("update-participant-status/:event_id/:participant_id/:status")
  async updateParticipantStatus(
    @Param("event_id") event_id: string,
    @Param("participant_id") participant_id: string,
    @Param("status") status: string,
    @Res() res: Response
  ) {
    try {
      let data = await this.participantService.updateParticipantStatus(
        event_id,
        participant_id,
        status
      );
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
