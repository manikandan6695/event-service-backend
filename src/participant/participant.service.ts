import { IEventModel } from "./../event/schema/event.schema";
import {
  CreateParticipantDTO,
  participantsDTO,
} from "./dto/create-participant.dto";
import { IParticipantModel } from "./schema/participant.schema";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { SharedService } from "src/shared/shared.service";
import { ReferenceService } from "src/reference/reference.service";
import { REQUEST_CONTEXT_ID } from "@nestjs/core/router/request/request-constants";
import { IInvitationModel } from "src/invitation/schema/invitation.schema";
import { EInvitationStatus } from "src/invitation/enum/invitation.enum";
const { ObjectId } = require("mongodb");
@Injectable()
export class ParticipantService {
  constructor(
    @InjectModel("participant")
    private readonly participantModel: mongoose.Model<IParticipantModel>,
    @InjectModel("event")
    private readonly eventModel: mongoose.Model<IEventModel>,
    @InjectModel("invitation")
    private readonly invitationModel: mongoose.Model<IInvitationModel>,
    private shared_service: SharedService,
    private reference_service: ReferenceService
  ) {}

  async addParticipant(body: any, created_by?: any, updated_by?: any) {
    try {
      for (let i = 0; i < body.participants.length; i++) {
        let participant_data = body.participants[i];
        let participant_fv = {
          sourceId: participant_data.sourceId,
          description: participant_data.description,
          sourceType: participant_data.sourceType,
          userId: participant_data.userId,
          created_by,
          updated_by,
        };

        if (participant_data.participantMode == "User") {
          participant_fv["participantStatus"] = "Applied";
          participant_fv["participantStatusHistory"] = [
            {
              status: "Applied",
              createdAt: new Date().toISOString(),
            },
          ];
          await this.invitationModel.updateOne(
            {
              sourceId: participant_data.sourceId,
              "userId.id": participant_data.userId.id,
            },
            { $set: { invitationStatus: EInvitationStatus.accepted } }
          );
        } else {
          participant_fv["participantStatus"] = "Confirmed";
          participant_fv["participantStatusHistory"] = [
            { status: "Confirmed", createdAt: new Date().toISOString() },
          ];
        }

        let participant = await this.participantModel.create(participant_fv);
      }

      await this.updateEventParticipants(body.participants[0].sourceId);

      return { message: "Created Successfully" };
    } catch (err) {
      throw err;
    }
  }

  async updateParticipantStatus(event_id, participant_id, status) {
    try {
      await this.participantModel.updateOne(
        { sourceId: event_id, _id: participant_id },
        { $set: { participantStatus: status } }
      );
      await this.updateEventParticipants(event_id);

      return { message: "Updated Successfully" };
    } catch (err) {
      throw err;
    }
  }

  async getEventByParticipants(userId: string, status: string) {
    try {
      let event_ids = await (
        await this.participantModel.find({
          "userId.id": userId,
          participantStatus: status,
        })
      ).map((e) => e.sourceId);
      console.log("event_ids", event_ids);

      return event_ids;
    } catch (err) {
      throw err;
    }
  }

  async updateEventParticipants(sourceId: string) {
    try {
      let participants = await this.participantModel.find({
        sourceId: sourceId,
      });
      console.log("partcipants", participants, sourceId);

      await this.eventModel.updateMany(
        { sourceId: sourceId },
        { $set: { participantDetails: participants } }
      );

      return { message: "Updated Successfully" };
    } catch (err) {
      throw err;
    }
  }

  async getParticipants(event_id: string) {
    try {
      let participants = await this.participantModel
        .find({ sourceId: event_id })
        .lean()
        .sort({ _id: -1 });
      for (let i = 0; i < participants.length; i++) {
        let curr_data = participants[i];

        let user = await this.reference_service.getUserById(
          curr_data.userId.id
        );
        if (user) {
          curr_data["user_detail"] = user;
        }
      }
      return participants;
    } catch (err) {
      throw err;
    }
  }
}
