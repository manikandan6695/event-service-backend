import * as mongoose from "mongoose";
import { ESource_type, ESSource_type } from "src/event/enum/event.enum";

export interface IParticipantStatusHistoryModel {
  status: string;
  createdAt: Date;
}
export interface IUserModel {
  id: string;
  user_name: string;
}
export interface IParticipantModel extends mongoose.Document {
  sourceId: any;
  sourceType: ESource_type;
  userId: IUserModel;
  role: string;
  description : string;
  participantStatus: string;
  participantStatusHistory: IParticipantStatusHistoryModel[];
  status: string;
  documentStatus: string;
  created_by: IUserModel;
  updated_by: IUserModel;
}

export const ParticipantStatusHistorySchema = new mongoose.Schema<any>({
  status: {
    type: String,
  },
  createdAt: {
    type: Date,
  },
});
export const UserSchema = new mongoose.Schema<any>({
  id: {
    type: String,
  },
  user_name: {
    type: String,
  },
});
export const ParticipantSchema = new mongoose.Schema<any>(
  {
    sourceId: {
      type: mongoose.Schema.Types.ObjectId,
      refPath: "sourceType",
    },
    sourceType: {
      type: String,
      enum: ESSource_type,
    },
    userId: UserSchema,
    role: {
      type: String,
    },
    description : {
      type : String
    },
    participantStatus: {
      type: String,
    },
    participantStatusHistory: [ParticipantStatusHistorySchema],
    status: {
      type: String,
      default : "Active"
    },
    documentStatus: {
      type: String,
      default : "Active"
    },
    created_by: UserSchema,
    updated_by: UserSchema,
  },
  {
    collection: "participant",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
