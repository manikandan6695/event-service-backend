import { Type } from "class-transformer";
import {
  IsArray,
  IsISO8601,
  IsMongoId,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";
import { UserIDDTO } from "src/event/dto/create-event.dto";

export class participantStatusHistoryDTO {
  @IsOptional()
  @IsString()
  status: string;

  @IsOptional()
  @IsISO8601()
  createdAt: string;
}
export class participantsDTO {
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateParticipantDTO)
  participants?: CreateParticipantDTO[];

  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  created_by?: UserIDDTO;

  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  updated_by?: UserIDDTO;
}
export class CreateParticipantDTO {
  @IsNotEmpty()
  @IsMongoId()
  @IsString()
  sourceId: string;

  @IsNotEmpty()
  @IsString()
  sourceType: string;

  @IsNotEmpty()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  userId: UserIDDTO;

  
  @IsNotEmpty()
  @IsString()
  participantMode : string; 
}
