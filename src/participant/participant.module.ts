import { EventSchema } from './../event/schema/event.schema';
import { ParticipantSchema } from "./schema/participant.schema";
import { Module } from "@nestjs/common";
import { ParticipantService } from "./participant.service";
import { ParticipantController } from "./participant.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { SharedModule } from "src/shared/shared.module";
import { ReferenceModule } from "src/reference/reference.module";
import { InvitationSchema } from 'src/invitation/schema/invitation.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "participant", schema: ParticipantSchema },
      { name: "event", schema: EventSchema },
      { name: "invitation", schema: InvitationSchema }
    ]),
    SharedModule,
    ReferenceModule,
  ],
  providers: [ParticipantService],
  controllers: [ParticipantController],
  exports: [ParticipantService],
})
export class ParticipantModule {}
