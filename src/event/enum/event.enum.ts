export enum ESource_type {
  event = "event",
  invitation = "invitation",
}

export const ESSource_type = [ESource_type.event, ESource_type.invitation];

export enum EEvent_status {
  Invited = "Invited",
  Applied = "Applied",
  Confirmed = "Confirmed",
  Active = "Active",
  Shortlist = "Shortlist",
  Closed = "Closed",
  Archived = "Archived",
  All = "All",
  Inactive = "Inactive",
}

export const ESEvent_status = [
  EEvent_status.Invited,
  EEvent_status.Applied,
  EEvent_status.Confirmed,
  EEvent_status.Shortlist,
  EEvent_status.Closed,
  EEvent_status.Inactive,
  EEvent_status.Archived,
  EEvent_status.Active,
  EEvent_status.All,
];
