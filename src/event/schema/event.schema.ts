import * as mongoose from "mongoose";

export interface ICategoryModel {
  category_id: string;
  category_name: string;
  category_key: string;
}
export interface IMedia {
  type?: string;
  media_id?: String;
  visibility?: string;
  media_url: string;
}

export interface ILanguageModel {
  language_id: string;
  language_code: string;
  language_name: string;
  language_native_name: string;
}

export interface IUserModel {
  id: string;
  user_name: string;
}
export interface ICityModel {
  city_name: string;
  city_id: string;
}
export interface IStateModel {
  state_name: string;
  state_id: string;
}
export interface ICountryModel {
  country_name: string;
  country_id: string;
}
export interface IAddressModel {
  address_first_line: string;
  address_second_line: string;
  city: ICityModel;
  state: IStateModel;
  country: ICountryModel;
  latitude: string;
  longitude: string;
}

// export interface IParticipantDetailsModel {
//   participantUserId: any;
//   participantStatus: string;
// }
export interface IEventModel extends mongoose.Document {
  name: string;
  type: string;
  sub_type: string;
  category: ICategoryModel;
  event_status: string;
  start_date: string;
  reporting_time : string;
  end_date?: string;
  description: string;
  tags: string[];
  link: string;
  additionalDetails: any;
  status: string;
  participantDetails: any[];
  documentStatus: string;
  media: IMedia[];
  language: ILanguageModel[];
  address: IAddressModel;
  created_by: IUserModel;
  updated_by: IUserModel;
}

export const UserSchema = new mongoose.Schema<any>({
  id: {
    type: String,
  },
  user_name: {
    type: String,
  },
});
export const LanguageSchema = new mongoose.Schema<any>({
  language_id: {
    type: String,
  },
  language_code: {
    type: String,
  },
  language_name: {
    type: String,
  },
  language_native_name: {
    type: String,
  },
});
export const MediaSchema = new mongoose.Schema({
  type: {
    type: String,
    description: "Types of the media",
  },
  visibility: {
    type: String,
    description: "Privacy settings of email",
    default: "Public",
  }, //public, private, mutual
  media_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "media",
  },
  media_url: {
    type: String,
  },
});
export const CountrySchema = new mongoose.Schema<any>({
  country_id: {
    type: String,
  },
  country_name: {
    type: String,
  },
});
export const StateSchema = new mongoose.Schema<any>({
  state_id: {
    type: String,
  },
  state_name: {
    type: String,
  },
});
export const CitySchema = new mongoose.Schema<any>({
  city_id: {
    type: String,
  },
  city_name: {
    type: String,
  },
});
export const CategorySchema = new mongoose.Schema<any>({
  category_id: {
    type: String,
  },
  category_name: {
    type: String,
  },
  category_key: {
    type: String,
  },
});
export const AddressSchema = new mongoose.Schema<any>({
  address_first_line: {
    type: String,
  },
  address_second_line: {
    type: String,
  },
  city: CitySchema,
  state: StateSchema,
  country: CountrySchema,
  pincode: {
    type: String,
  },
  latitude: {
    type: String,
  },
  longitude: {
    type: String,
  },
});
// export const ParticipantDetailsSchema = new mongoose.Schema<any>({
//   participantUserId: {
//     type: String,
//   },
//   participantStatus: {
//     type: String,
//   },
// });
export const EventSchema = new mongoose.Schema<any>(
  {
    name: {
      type: String,
    },
    type: {
      type: String,
    },
    sub_type: {
      type: String,
    },
    category: CategorySchema,
    reporting_time : {
      type: Date,
    },
    event_status: {
      type: String,
      default: "Active"
    },
    start_date: {
      type: Date,
    },
    end_date: {
      type: Date,
    },
    description: {
      type: String,
    },
    tags: [
      {
        type: String,
      },
    ],
    link: {
      type: String,
    },
    additionalDetails: {
      type: String,
    },
    status: {
      type: String,
      default: "Active",
    },
    documentStatus: {
      type: String,
      default: "Active",
    },
    media: [MediaSchema],
    participantDetails: [
      {
        type: mongoose.Schema.Types.Mixed,
      },
    ],
    language: [LanguageSchema],
    address: AddressSchema,
    created_by: UserSchema,
    updated_by: UserSchema,
  },
  {
    collection: "event",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
