import { EEvent_status, ESource_type } from "src/event/enum/event.enum";
import { ParticipantService } from "./../participant/participant.service";
import { InvitationService } from "./../invitation/invitation.service";
import { CreateEventDTO } from "./dto/create-event.dto";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { ReferenceService } from "src/reference/reference.service";
import { SharedService } from "src/shared/shared.service";
import { IEventModel } from "./schema/event.schema";
const { ObjectId } = require("mongodb");

@Injectable()
export class EventService {
  constructor(
    @InjectModel("event")
    private readonly eventModel: mongoose.Model<IEventModel>,
    private shared_service: SharedService,
    private invitation_service: InvitationService,
    private participant_service: ParticipantService,
    private reference_service: ReferenceService
  ) {}

  async createEvent(body: CreateEventDTO, created_by: any, updated_by: any) {
    try {
      let fv = {
        ...body,
        created_by,
        updated_by,
      };
      let event = await this.eventModel.create(fv);

      if (body.invitees.length) {
        body.invitees.forEach((e) => {
          e["sourceId"] = event._id;
          e["sourceType"] = ESource_type.event;
        });

        await this.invitation_service.addInvitation(body);
      }

      if (body.participants.length) {
        body.participants.forEach((e) => {
          e["sourceId"] = event._id;
          e["sourceType"] = ESource_type.event;
        });
        await this.participant_service.addParticipant(
          body,
          created_by,
          updated_by
        );
      }

      return event;
    } catch (err) {
      throw err;
    }
  }

  async getEventList(
    skip: number,
    limit: number,
    search: string,
    category_id: string[],
    language_id: string[],
    city_id: string[],
    start_date: string,
    end_date: string,
    user_id: string,
    status: string
  ) {
    try {
      let aggregation_pipeline: any[] = [{ $sort: { _id: -1 } }];
      aggregation_pipeline.push({
        $match: {
          documentStatus: EEvent_status.Active,
          event_status: EEvent_status.Active,
        },
      });
      if (status != EEvent_status.All) {
        let event_ids;

        if (status == EEvent_status.Invited) {
          event_ids = await this.invitation_service.getEventByInvitation(
            user_id
          );
        }
        if (status == EEvent_status.Applied) {
          event_ids = await this.participant_service.getEventByParticipants(
            user_id,
            status
          );
        }
        if (status == EEvent_status.Confirmed) {
          event_ids = await this.participant_service.getEventByParticipants(
            user_id,
            status
          );
        }
        if (status == EEvent_status.Shortlist) {
          event_ids = await this.participant_service.getEventByParticipants(
            user_id,
            status
          );
        }
        if (status == EEvent_status.Archived) {
          aggregation_pipeline.push({
            $match: {
              end_date: { $gte: new Date() },
            },
          });

          aggregation_pipeline.push({
            $match: {
              $or: [
                { end_date: { $gte: new Date() } },
                { documentStatus: EEvent_status.Inactive },
              ],
            },
          });
        }

        aggregation_pipeline.push({
          $match: {
            _id: { $in: event_ids.map((e) => new mongoose.Types.ObjectId(e)) },
          },
        });
      }
      console.log("aggregation_pipeline", JSON.stringify(aggregation_pipeline));

      if (search) {
        aggregation_pipeline.push({
          $match: {
            name: new RegExp(search, "i"),
          },
        });
      }

      if (start_date && end_date) {
        aggregation_pipeline.push({
          $match: { start_date: { $gte: new Date(start_date) } },
        });
        aggregation_pipeline.push({
          $match: { end_date: { $lte: new Date(end_date) } },
        });
      }

      if (category_id.length) {
        let multi = typeof category_id != "string";
        let category;
        if (multi) {
          category = category_id.map((e) => e);
        } else {
          category = category_id;
        }
        aggregation_pipeline.push({
          $match: {
            "category.category_id": multi
              ? { $in: category }
              : { $in: [category] },
          },
        });
      }

      if (language_id.length) {
        let multi = typeof language_id != "string";
        let language;
        if (multi) {
          language = language_id.map((e) => e);
        } else {
          language = language_id;
        }
        aggregation_pipeline.push({
          $match: {
            "language.language_id": multi
              ? { $in: language }
              : { $in: [language] },
          },
        });
      }

      if (city_id.length) {
        let multi = typeof city_id != "string";
        let city;
        if (multi) {
          city = city_id.map((e) => e);
        } else {
          city = city_id;
        }
        aggregation_pipeline.push({
          $match: {
            "address.city.city_id": {
              $in: city,
            },
          },
        });
      }

      let data = await this.eventModel.aggregate(aggregation_pipeline);
      let count_pipeline = [
        ...aggregation_pipeline,
        { $group: { _id: null, count_detail: { $sum: 1 } } },
      ];
      aggregation_pipeline.push(
        {
          $sort: {
            _id: -1,
          },
        },
        {
          $skip: skip,
        },
        {
          $limit: limit,
        }
      );
      let count_data = await this.eventModel.aggregate(count_pipeline);
      let count;
      if (count_data.length) {
        count = count_data[0].count_detail;
      }

      return { data, count };
    } catch (err) {
      throw err;
    }
  }

  async getEventDetail(id: string, user_id: string) {
    try {
      let data = await this.eventModel.findOne({ _id: id }).lean();
      let participants = await this.participant_service.getParticipants(id);
      // let user = await this.reference_service.getUserById(data.created_by.id);
      // data["user_detail"] = user;
      console.log("user_id", user_id);

      if (user_id) {
        let validateInvitation =
          await this.invitation_service.validateInvitation(user_id, id);
        console.log("validateInvitation", validateInvitation);

        if (validateInvitation) data["enable_apply"] = true;
        else data["enable_apply"] = false;
      }
      data["participants"] = participants;
      return data;
    } catch (err) {
      throw err;
    }
  }

  async updateEvent(
    id: string,
    body: CreateEventDTO,
    created_by: any,
    updated_by: any
  ) {
    try {
      let fv = {
        ...body,
        created_by,
        updated_by,
      };

      let data = await this.eventModel.updateOne({ _id: id }, fv);

      return data;
    } catch (err) {
      throw err;
    }
  }

  async updateEventStatus(id: string, status: string) {
    try {
      await this.eventModel.updateOne(
        { _id: id },
        { $set: { event_status: status } }
      );
      return { message: "Updated Successfully" };
    } catch (err) {
      throw err;
    }
  }
}
