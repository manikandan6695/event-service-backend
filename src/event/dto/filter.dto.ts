import {
  IsArray,
  IsEnum,
  IsISO8601,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from "class-validator";
import { EEvent_status } from "../enum/event.enum";

export class FilterDTO {
  @IsOptional()
  @IsString()
  search?: string;

  @IsOptional()
  @IsArray()
  @IsMongoId({ each: true })
  category_id?: string[];

  @IsOptional()
  @IsArray()
  @IsMongoId({ each: true })
  language_id?: string[];

  @IsOptional()
  @IsArray()
  @IsMongoId({ each: true })
  city_id?: string[];

  @IsNotEmpty()
  @IsNumber()
  skip: number;

  @IsNotEmpty()
  @IsNumber()
  limit: number;

  @IsOptional()
  @IsISO8601()
  @IsString()
  start_date?: string;

  @IsOptional()
  @IsISO8601()
  @IsString()
  end_date?: string;

  @IsOptional()
  @IsString()
  user_id: string;

  @IsOptional()
  @IsString()
  @IsEnum(EEvent_status)
  status: EEvent_status;
}
