import { Type } from "class-transformer";
import {
  IsArray,
  IsISO8601,
  IsMongoId,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";

export class UserIDDTO {
  @IsNotEmpty()
  @IsMongoId()
  @IsString()
  id: string;

  @IsNotEmpty()
  @IsString()
  user_name: string;
}
export class CategoryDTO {
  @IsNotEmpty()
  @IsString()
  category_id: string;

  @IsNotEmpty()
  @IsString()
  category_name: string;
}

export class LanguageDTO {
  @IsNotEmpty()
  @IsString()
  language_id: string;

  @IsNotEmpty()
  @IsString()
  language_name: string;
}
export class CityDTO {
  @IsNotEmpty()
  @IsString()
  city_id: string;

  @IsNotEmpty()
  @IsString()
  city_name: string;
}

export class StateDTO {
  @IsNotEmpty()
  @IsString()
  state_id: string;

  @IsNotEmpty()
  @IsString()
  state_name: string;
}
export class MediaDTO {
  @IsNotEmpty()
  @IsString()
  language_id: string;

  @IsNotEmpty()
  @IsString()
  language_name: string;
}
export class CountryDTO {
  @IsNotEmpty()
  @IsString()
  country_id: string;

  @IsNotEmpty()
  @IsString()
  country_name: string;
}

export class RefMediaDTO {
  @IsNotEmpty()
  @IsString()
  type: string;

  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  media_id: string;

  @IsOptional()
  @IsString()
  media_url?: string;

  @IsOptional()
  @IsString()
  visibility?: string;
}
export class AddressDTO {
  @IsOptional()
  @IsString()
  address_first_line: string;

  @IsOptional()
  @IsString()
  address_second_line: string;

  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => CityDTO)
  city: CityDTO;

  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => StateDTO)
  state: StateDTO;

  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => CountryDTO)
  country: CountryDTO;

  @IsOptional()
  @IsString()
  latitude: string;

  @IsOptional()
  @IsString()
  longitude: string;
}

export class invitationDTO {
  @IsNotEmpty()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  userId: UserIDDTO;

  @IsNotEmpty()
  @IsString()
  acceptanceMode: string;
}

export class participantsDTO {
  @IsNotEmpty()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  userId: UserIDDTO;

  @IsNotEmpty()
  @IsString()
  participantMode: string;
}

export class CreateEventDTO {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  @IsMongoId()
  id: string;

  @IsNotEmpty()
  @IsString()
  type: string;

  @IsNotEmpty()
  @IsISO8601()
  start_date: Date;

  @IsOptional()
  @IsISO8601()
  reporting_time : Date;

  @IsNotEmpty()
  @IsISO8601()
  end_date: Date;

  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => AddressDTO)
  address: AddressDTO;

  @IsNotEmpty()
  @IsObject()
  @ValidateNested()
  @Type(() => CategoryDTO)
  category: CategoryDTO;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => LanguageDTO)
  language: LanguageDTO[];

  @IsOptional()
  @IsString()
  link: string;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => RefMediaDTO)
  media?: RefMediaDTO[];

  @IsOptional()
  @IsArray()
  tags: string;

  @IsNotEmpty()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  created_by: UserIDDTO;

  @IsNotEmpty()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  updated_by: UserIDDTO;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => participantsDTO)
  participants?: participantsDTO[];

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => invitationDTO)
  invitees?: invitationDTO[];
}
