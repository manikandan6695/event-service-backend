import { FilterDTO } from "./dto/filter.dto";
import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  Res,
  ValidationPipe,
} from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { EventService } from "./event.service";
import { Response } from "express";
import { CreateEventDTO } from "./dto/create-event.dto";

@Controller("event")
export class EventController {
  constructor(
    private readonly eventService: EventService,
    private sservice: SharedService
  ) {}

  @Post("create-event")
  async createEvent(
    @Body(new ValidationPipe()) body: CreateEventDTO,
    @Res() res: Response
  ) {
    try {
      let event = await this.eventService.createEvent(
        body,
        body.created_by,
        body.updated_by
      );
      return res.json(event);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @Post("get-event-list")
  async getEventList(
    @Body(new ValidationPipe()) query: FilterDTO,
    @Res() res: Response
  ) {
    try {
      let data = await this.eventService.getEventList(
        query.skip,
        query.limit,
        query.search,
        query.category_id,
        query.language_id,
        query.city_id,
        query.start_date,
        query.end_date,
        query.user_id,
        query.status
      );
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @Get("get-event-detail/:id")
  async getEventDetail(
    @Param("id") id: string,
    @Query("user_id") user_id: string,
    @Res() res: Response
  ) {
    try {
      let data = await this.eventService.getEventDetail(id,user_id);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }

  @Patch("update-event/:id")
  async updateEvent(
    @Param("id") id: string,
    @Body(new ValidationPipe()) body: CreateEventDTO,
    @Res() res: Response
  ) {
    try {
      let data = await this.eventService.updateEvent(
        id,
        body,
        body.created_by,
        body.updated_by
      );
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
  @Patch("update-event-status/:id/:status")
  async updateEventStatus(
    @Param("id") id: string,
    @Param("status") status: string,
    @Res() res: Response
  ) {
    try {
      let data = await this.eventService.updateEventStatus(id, status);
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
}
