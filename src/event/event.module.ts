import { InvitationModule } from './../invitation/invitation.module';
import { ParticipantModule } from './../participant/participant.module';
import { Module } from "@nestjs/common";
import { EventService } from "./event.service";
import { EventController } from "./event.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { SharedModule } from "src/shared/shared.module";
import { ReferenceModule } from "src/reference/reference.module";
import { EventSchema } from "./schema/event.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "event", schema: EventSchema }]),
    SharedModule,
    ReferenceModule,
    ParticipantModule,
    InvitationModule
  ],
  controllers: [EventController],
  providers: [EventService],
  exports: [EventService],
})
export class EventModule {}
