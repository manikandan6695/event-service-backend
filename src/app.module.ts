import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { MongooseModule } from "@nestjs/mongoose";
import { ScheduleModule } from "@nestjs/schedule";
import { SharedModule } from "./shared/shared.module";
import { ReferenceModule } from './reference/reference.module';
import { EventModule } from './event/event.module';
import { ParticipantModule } from "./participant/participant.module";
// import { ParticipantModule } from './participant/participant.module';
import { InvitationModule } from './invitation/invitation.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, envFilePath: ".env" }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => {
        return {
          uri: config.get("DB_URL"),
          useNewUrlParser: true,
        };
      },
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
    SharedModule,
    ReferenceModule,
    EventModule,
    ParticipantModule,
    InvitationModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
