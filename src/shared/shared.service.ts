import { HttpStatus, Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { AppException } from "./app-exception";
@Injectable()
export class SharedService {
  defaultLanguage = "en";
  defaultExtension = "+91";
  // options can be passed, e.g. {allErrors: true}
  schemas = {};
  constructor(private config: ConfigService) {}

  processError(err: Error, context: string) {
    let code: HttpStatus, response;
    if (err instanceof AppException) {
      code = err.getCode();
      response = { code, message: err.getMessage() };
    } else {
      if (err.name == "MongoError") {
        if (err["code"] == 11000) {
          let prop = "Value";
          try {
            prop = err.message.split(":")[2].replace(" dup key", "").trim();
          } catch (properr) {
            console.log("cant get prop");
          }
          code = HttpStatus.NOT_ACCEPTABLE;
          response = { code, message: `${prop} provided already exist` };
        } else {
          code = HttpStatus.INTERNAL_SERVER_ERROR;
          response = { code, message: err.message };
        }
      } else {
        code = HttpStatus.INTERNAL_SERVER_ERROR;
        response = { code, message: err.message };
      }
    }
    console.log("going to process err", err.stack);
    return { code, response };
  }
}
export enum app_date_ranges {
  all = "all",
  this_month = "this_month",
  this_week = "this_week",
  yesterday = "yesterday",
  today = "today",
  last_seven_days = "last_seven_days",
  last_three_months = "last_three_months",
  last_six_months = "last_six_months",
  last_twelve_months = "last_twelve_months",
  this_year = "this_year",
  this_financial_year = "this_financial_year",
  last_financial_year = "last_financial_year",
  last_year = "last_year",
  custom = "custom",
}

export enum app_report_key {
  sales_by_customer = "sales-by-customer",
  sales_by_branch = "sales-by-branch",
  sales_by_item = "sales-by-item",
  invoice_report = "invoice-report",
  payment_received_report = "payment-received-report",
  customer_balance = "customer-balance",
  inventory_summary_report = "inventory-summary-report",
}
export const app_variables = {
  default_language_id: "6091525efccd22ae080d9107",
  default_db_date_format_id: "6177bd95c8f14ed85c69aba8",
  default_db_date_format: "%d/%m/%Y",
  default_decimal_digits: 2,
  default_org_fcode: "organization",
  default_role_fcode: "ROLE",
  default_branch_fcode: "BRCH",
  default_warehouse_fcode: "WREHS",
  default_customer_fcode: "CUST",
  default_ecom_customer_fcode: "CUSTB2C",
  default_sales_person_fcode: "SLP",
  default_vendor_fcode: "VEND",
  default_user_fcode: "USR",
  support_ticket_fcode: "SPT",
  support_ticket_conv_fcode: "STC",
  item_fcode: "ITM",
  inquiry_fcode: "INQ",
  estimation_fcode: "EST",
  invoice_fcode: "INVO",
  sale_order_fcode: "SO",
  payment_received_fcode: "PMTR",
  package_fcode: "PACK",
  shipment_fcode: "SHIP",
  channel_digital_catalogue: "digital_catalogue",
  channel_online_store: "online_store",
  channel_wholesale: "wholesale",
  channel_retail: "retail",
  default_item_category_type: "items",
  default_subscription_fcode: "subscription",
  default_transfer_order_fcode: "transfer-order",
  default_deals_fcode: "deals",
  default_inventory_adjustment_fcode: "inventory-adjustment",
  default_calls_fcode: "calls",
  default_opening_balance_fcode: "opening-balance",
  default_opening_balance_detail_fcode: "opening-balance-detail",
  default_classification_fcode: "classification",
  default_merchant_fcode: "merchant",
  default_expenses_fcode: "expenses",
  default_expense_report_fcode: "expense-report",
  default_expense_tag_fcode: "expense-tag",
  default_general_ledger_fcode: "general-ledger",
  default_reason_fcode: "reason",
  default_purchase_order_fcode: "purchase-order",
  default_credit_note_fcode: "credit-notes",
  default_bills_fcode: "bills",
  default_payment_made_fcode: "PMTM",
  default_tds_fcode: "tds",
  default_tcs_fcode: "tcs",
  default_account_transaction_fcode: "account-transaction",
  default_refund_fcode: "refund",
  default_online_store_fcode: "online-store",
  default_collection_fcode: "collection",
  default_sort_fcode: "sort",
  default_discount_fcode: "discount",
  default_legals_fcode: "legal",
  default_reviews_fcode: "reviews",
  default_review_response_fcode: "review-response",
  default_nav_menu_fcode: "nav-menu",
  default_nav_menu_items_fcode: "nav-menu-items",
  default_sale_return_fcode: "sale-return",
  default_receive_fcode: "return-receipt",
  default_shipping_zone_fcode: "shipping-zone",
  default_shipping_region_fcode: "shipping-region",
  default_shipping_rate_fcode: "shipping-rate",
  default_cart_fcode: "cart",
  default_ecom_offline_payments_fcode: "ecom-offline-payments",
  default_app_credential_fcode: "app-credential",
  default_sn_org_profile_fcode: "sn-org-profile",
  default_feature_request_fcode: "feature-request",
  default_sa_feature_request_fcode: "sa-feature-request",
};

export enum activity_icon {
  status_change = "feather icon-refresh-ccw",
  assigned_to_user = "feather icon-user-check",
  file_text = "feather icon-file-text",
}
export interface SectionFields {
  field: string;
  field_name: string;
  is_unique: boolean;
  is_required: boolean;
}

export enum TypeMapping {
  string = "isString",
  objectid = "isMongoId",
  boolean = "isBooleanString",
  number = "isNumber",
}
