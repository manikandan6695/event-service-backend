import { Global, Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { SharedService } from "./shared.service";
@Global()
@Module({
  imports: [MongooseModule.forFeature([])],
  providers: [SharedService],
  exports: [SharedService],
  controllers: [],
})
export class SharedModule {}
