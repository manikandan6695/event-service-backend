import * as mongoose from "mongoose";
import { ESource_type, ESSource_type } from "src/event/enum/event.enum";


export interface IUserModel {
  id: string;
  user_name: string;
}
export interface IInvitationModel extends mongoose.Document {
  sourceId: any;
  sourceType: ESource_type;
  userId: IUserModel;
  role: string;
  acceptanceMode: string;
  invitationStatus: string;
  status: string;
  documentStatus: string;
  created_by: IUserModel;
  updated_by: IUserModel;
}

export const UserSchema = new mongoose.Schema<any>({
  id: {
    type: String,
  },
  user_name: {
    type: String,
  },
});
export const InvitationSchema = new mongoose.Schema<any>(
  {
    sourceId: {
      type: mongoose.Schema.Types.ObjectId,
      refPath: "sourceType",
    },
    sourceType: {
      type: String,
      enum: ESSource_type,
    },
    userId: UserSchema,
    role: {
      type: String,
    },
    acceptanceMode: {
      type: String,
    },
    invitationStatus: {
      type: String,
    },
    status: {
      type: String,
      default: "Active",
    },
    documentStatus: {
      type: String,
      default: "Active",
    },
    created_by: UserSchema,
    updated_by: UserSchema,
  },
  {
    collection: "invitation",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
  }
);
