export enum EInvitationStatus {
    pending = "Pending",
    accepted = "Accepted",
  }
  
  export const ESInvitationStatus = [EInvitationStatus.pending, EInvitationStatus.accepted];