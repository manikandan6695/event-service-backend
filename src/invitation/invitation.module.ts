import { InvitationSchema } from "./schema/invitation.schema";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { InvitationController } from "./invitation.controller";
import { InvitationService } from "./invitation.service";
import { SharedModule } from "src/shared/shared.module";
import { ReferenceModule } from "src/reference/reference.module";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "invitation", schema: InvitationSchema }]),
    SharedModule,
    ReferenceModule,
  ],
  controllers: [InvitationController],
  providers: [InvitationService],
  exports: [InvitationService],
})
export class InvitationModule {}
