import { ParticipantSchema } from './../participant/schema/participant.schema';
import { InvitationService } from "./invitation.service";
import { Body, Controller, Post, ValidationPipe, Res, Get } from "@nestjs/common";
import { SharedService } from "src/shared/shared.service";
import { CreateInvitationDTO, InvitationDTO } from "./dto/create-invitation.dto";
import { Response } from "express";

@Controller("invitation")
export class InvitationController {
  constructor(
    private readonly invitationService: InvitationService,
    private sservice: SharedService
  ) {}
  @Post("add-invitation")
  async addInvitation(
    @Body(new ValidationPipe()) body: InvitationDTO,
    @Res() res: Response
  ) {
    try {
      let data = await this.invitationService.addInvitation(
        body,
        body.created_by,
        body.updated_by
      );
      return res.json(data);
    } catch (err) {
      const { code, response } = await this.sservice.processError(
        err,
        this.constructor.name
      );
      return res.status(code).json(response);
    }
  }
  
 
}
