import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import * as mongoose from "mongoose";
import { ReferenceService } from "src/reference/reference.service";
import { SharedService } from "src/shared/shared.service";
import { CreateInvitationDTO } from "./dto/create-invitation.dto";
import { EInvitationStatus } from "./enum/invitation.enum";
import { IInvitationModel } from "./schema/invitation.schema";
const { ObjectId } = require("mongodb");
@Injectable()
export class InvitationService {
  constructor(
    @InjectModel("invitation")
    private readonly invitationModel: mongoose.Model<IInvitationModel>,
    private shared_service: SharedService,
    private reference_service: ReferenceService
  ) {}

  async addInvitation(body: any, created_by?: any, updated_by?: any) {
    try {
      for (let i = 0; i < body.invitees.length; i++) {
        let invitee_data = body.invitees[i];
        let fv = {
          sourceId: invitee_data.sourceId,
          sourceType: invitee_data.sourceType,
          userId: invitee_data.userId,
          invitationStatus: "Pending",
          acceptanceMode: invitee_data.acceptanceMode,
          created_by,
          updated_by,
        };
        await this.invitationModel.create(fv);
      }

      return { message: "Invited Successfully" };
    } catch (err) {
      throw err;
    }
  }

  async validateInvitation(user_id: string, event_id: string) {
    try {
      let data = await this.invitationModel.findOne({
        "userId.id": user_id,
        sourceId: event_id,
        invitationStatus: EInvitationStatus.pending,
      });

      return data;
    } catch (err) {
      throw err;
    }
  }

  async getEventByInvitation(userId: string) {
    try {
      let event_ids = await (
        await this.invitationModel.find({ "userId.id": userId })
      ).map((e) => e.sourceId);
      console.log("event_ids", event_ids);

      return event_ids;
    } catch (err) {
      throw err;
    }
  }
}
