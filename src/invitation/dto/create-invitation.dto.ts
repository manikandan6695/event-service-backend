import { Type } from "class-transformer";
import {
  IsArray,
  IsMongoId,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
  ValidateNested,
} from "class-validator";
import { UserIDDTO } from "src/event/dto/create-event.dto";

export class InvitationDTO {
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateInvitationDTO)
  invitees?: CreateInvitationDTO[];

  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  created_by?: UserIDDTO;

  @IsOptional()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  updated_by?: UserIDDTO;
}
export class CreateInvitationDTO {
  @IsNotEmpty()
  @IsMongoId()
  @IsString()
  sourceId: string;

  @IsNotEmpty()
  @IsString()
  sourceType: string;

  @IsNotEmpty()
  @IsObject()
  @ValidateNested()
  @Type(() => UserIDDTO)
  userId: UserIDDTO;

  @IsNotEmpty()
  @IsString()
  acceptanceMode: string;
}
