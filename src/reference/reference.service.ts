import { Injectable } from "@nestjs/common";
import { HttpService } from "@nestjs/axios";
import { SharedService } from "src/shared/shared.service";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class ReferenceService {
  constructor(
    private http_service: HttpService,
    private config_service: ConfigService,
    private shared_service: SharedService
  ) {}

  async getUserById(user_id: string) {
    try {
      let body = {
        user_id: user_id,
      };
      console.log("body is", body);
      console.log("url is", this.config_service.get("CASTTREE_BASE_URL"));
      
      let data = await this.http_service
        .post(
          `${this.config_service.get(
            "CASTTREE_BASE_URL"
          )}/user/get-user-detail`,
          body
        )
        .toPromise();
     
      console.log("data is", data);

      return data.data;
    } catch (err) {
      throw err;
    }
  }

  async updateUserDetail(user_id: string, phone_number: string, email: string) {
    try {
      let body = {
        user_id: user_id,
        phone_number: phone_number,
        email: email,
      };

      let data = await this.http_service
        .post(
          `${this.config_service.get(
            "CASTTREE_BASE_URL"
          )}/user/update-user-detail`,
          body
        )
        .toPromise();

      return data.data;
    } catch (err) {
      throw err;
    }
  }
}
