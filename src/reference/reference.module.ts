import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { SharedModule } from "src/shared/shared.module";
import { ReferenceService } from "./reference.service";

@Module({
  imports: [HttpModule, SharedModule, ConfigModule],
  controllers: [],
  providers: [ReferenceService],
  exports: [ReferenceService],
})
export class ReferenceModule {}
