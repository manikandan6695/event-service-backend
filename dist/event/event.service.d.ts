/// <reference types="mongoose/types/aggregate" />
/// <reference types="mongoose/types/callback" />
/// <reference types="mongoose/types/collection" />
/// <reference types="mongoose/types/connection" />
/// <reference types="mongoose/types/cursor" />
/// <reference types="mongoose/types/document" />
/// <reference types="mongoose/types/error" />
/// <reference types="mongoose/types/expressions" />
/// <reference types="mongoose/types/helpers" />
/// <reference types="mongoose/types/middlewares" />
/// <reference types="mongoose/types/indexes" />
/// <reference types="mongoose/types/models" />
/// <reference types="mongoose/types/mongooseoptions" />
/// <reference types="mongoose/types/pipelinestage" />
/// <reference types="mongoose/types/populate" />
/// <reference types="mongoose/types/query" />
/// <reference types="mongoose/types/schemaoptions" />
/// <reference types="mongoose/types/schematypes" />
/// <reference types="mongoose/types/session" />
/// <reference types="mongoose/types/types" />
/// <reference types="mongoose/types/utility" />
/// <reference types="mongoose/types/validation" />
/// <reference types="mongoose/types/virtuals" />
/// <reference types="mongoose/types/inferschematype" />
import { ParticipantService } from "./../participant/participant.service";
import { InvitationService } from "./../invitation/invitation.service";
import { CreateEventDTO } from "./dto/create-event.dto";
import { Model } from "mongoose";
import { ReferenceService } from "src/reference/reference.service";
import { SharedService } from "src/shared/shared.service";
import { IEventModel } from "./schema/event.schema";
export declare class EventService {
    private readonly eventModel;
    private shared_service;
    private invitation_service;
    private participant_service;
    private reference_service;
    constructor(eventModel: Model<IEventModel>, shared_service: SharedService, invitation_service: InvitationService, participant_service: ParticipantService, reference_service: ReferenceService);
    createEvent(body: CreateEventDTO, created_by: any, updated_by: any): Promise<IEventModel & {
        _id: import("mongoose").Types.ObjectId;
    }>;
    getEventList(skip: number, limit: number, search: string, category_id: string[], language_id: string[], city_id: string[], start_date: string, end_date: string): Promise<{
        data: any[];
        count: any;
    }>;
    getEventDetail(id: string): Promise<import("mongoose").LeanDocument<IEventModel & {
        _id: import("mongoose").Types.ObjectId;
    }>>;
    updateEvent(id: string, body: CreateEventDTO, created_by: any, updated_by: any): Promise<import("mongodb").UpdateResult>;
    updateEventStatus(id: string, status: string): Promise<{
        message: string;
    }>;
}
