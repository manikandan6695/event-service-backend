export declare class UserIDDTO {
    id: string;
    user_name: string;
}
export declare class CategoryDTO {
    category_id: string;
    category_name: string;
}
export declare class LanguageDTO {
    language_id: string;
    language_name: string;
}
export declare class CityDTO {
    city_id: string;
    city_name: string;
}
export declare class StateDTO {
    state_id: string;
    state_name: string;
}
export declare class MediaDTO {
    language_id: string;
    language_name: string;
}
export declare class CountryDTO {
    country_id: string;
    country_name: string;
}
export declare class RefMediaDTO {
    type: string;
    media_id: string;
    media_url?: string;
    visibility?: string;
}
export declare class AddressDTO {
    address_first_line: string;
    address_second_line: string;
    city: CityDTO;
    state: StateDTO;
    country: CountryDTO;
    latitude: string;
    longitude: string;
}
export declare class invitationDTO {
    userId: UserIDDTO;
    acceptanceMode: string;
}
export declare class participantsDTO {
    userId: UserIDDTO;
    participantMode: string;
}
export declare class CreateEventDTO {
    name: string;
    id: string;
    type: string;
    start_date: Date;
    end_date: Date;
    address: AddressDTO;
    category: CategoryDTO;
    description: string;
    language: LanguageDTO[];
    link: string;
    media?: RefMediaDTO[];
    tags: string;
    created_by: UserIDDTO;
    updated_by: UserIDDTO;
    participants?: participantsDTO[];
    invitees?: invitationDTO[];
}
