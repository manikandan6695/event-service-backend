"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventService = void 0;
const event_enum_1 = require("./enum/event.enum");
const participant_service_1 = require("./../participant/participant.service");
const invitation_service_1 = require("./../invitation/invitation.service");
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const reference_service_1 = require("../reference/reference.service");
const shared_service_1 = require("../shared/shared.service");
let EventService = class EventService {
    constructor(eventModel, shared_service, invitation_service, participant_service, reference_service) {
        this.eventModel = eventModel;
        this.shared_service = shared_service;
        this.invitation_service = invitation_service;
        this.participant_service = participant_service;
        this.reference_service = reference_service;
    }
    async createEvent(body, created_by, updated_by) {
        try {
            let fv = Object.assign(Object.assign({}, body), { created_by,
                updated_by });
            let event = await this.eventModel.create(fv);
            if (body.invitees.length) {
                body.invitees.forEach((e) => {
                    e["sourceId"] = event._id;
                    e["sourceType"] = event_enum_1.ESource_type.event;
                });
                await this.invitation_service.addInvitation(body);
            }
            if (body.participants.length) {
                body.participants.forEach((e) => {
                    e["sourceId"] = event._id;
                    e["sourceType"] = event_enum_1.ESource_type.event;
                });
                await this.participant_service.addParticipant(body, created_by, updated_by);
            }
            return event;
        }
        catch (err) {
            throw err;
        }
    }
    async getEventList(skip, limit, search, category_id, language_id, city_id, start_date, end_date) {
        try {
            let aggregation_pipeline = [{ $sort: { _id: -1 } }];
            aggregation_pipeline.push({ $match: { documentStatus: "Active" } });
            if (search) {
                aggregation_pipeline.push({
                    $match: {
                        name: new RegExp(search, "i"),
                    },
                });
            }
            if (start_date && end_date) {
                aggregation_pipeline.push({
                    $match: { start_date: { $gte: new Date(start_date) } },
                });
                aggregation_pipeline.push({
                    $match: { end_date: { $lte: new Date(end_date) } },
                });
            }
            if (category_id.length) {
                console.log("category_id is", category_id);
                let multi = typeof category_id != "string";
                console.log("multi is", multi);
                let category;
                if (multi) {
                    category = category_id.map((e) => e);
                }
                else {
                    console.log("inside else", category_id);
                    category = category_id;
                    console.log("inside else category", category, category_id);
                }
                console.log("category is", category, multi);
                aggregation_pipeline.push({
                    $match: {
                        "category.category_id": multi
                            ? { $in: category }
                            : { $in: [category] },
                    },
                });
            }
            if (language_id.length) {
                console.log("inside language", language_id.length);
                let multi = typeof language_id != "string";
                let language;
                if (multi) {
                    language = language_id.map((e) => e);
                }
                else {
                    language = language_id;
                }
                aggregation_pipeline.push({
                    $match: {
                        "language.language_id": multi
                            ? { $in: language }
                            : { $in: [language] },
                    },
                });
            }
            if (city_id.length) {
                let multi = typeof city_id != "string";
                let city;
                if (multi) {
                    city = city_id.map((e) => e);
                }
                else {
                    city = city_id;
                }
                aggregation_pipeline.push({
                    $match: {
                        "address.city.city_id": {
                            $in: city,
                        },
                    },
                });
            }
            console.log("aggregation pipeline is", JSON.stringify(aggregation_pipeline));
            let data = await this.eventModel.aggregate(aggregation_pipeline);
            let count_pipeline = [
                ...aggregation_pipeline,
                { $group: { _id: null, count_detail: { $sum: 1 } } },
            ];
            aggregation_pipeline.push({
                $sort: {
                    _id: -1,
                },
            }, {
                $skip: skip,
            }, {
                $limit: limit,
            });
            let count_data = await this.eventModel.aggregate(count_pipeline);
            let count;
            if (count_data.length) {
                count = count_data[0].count_detail;
            }
            return { data, count };
        }
        catch (err) {
            throw err;
        }
    }
    async getEventDetail(id) {
        try {
            let data = await this.eventModel.findOne({ _id: id }).lean();
            let participants = await this.participant_service.getParticipants(id);
            data["participants"] = participants;
            return data;
        }
        catch (err) {
            throw err;
        }
    }
    async updateEvent(id, body, created_by, updated_by) {
        try {
            let fv = Object.assign(Object.assign({}, body), { created_by,
                updated_by });
            let data = await this.eventModel.updateOne({ _id: id }, fv);
            return data;
        }
        catch (err) {
            throw err;
        }
    }
    async updateEventStatus(id, status) {
        try {
            await this.eventModel.updateOne({ _id: id }, { $set: { event_status: status } });
            return { message: "Updated Successfully" };
        }
        catch (err) {
            throw err;
        }
    }
};
EventService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)("event")),
    __metadata("design:paramtypes", [mongoose_2.Model,
        shared_service_1.SharedService,
        invitation_service_1.InvitationService,
        participant_service_1.ParticipantService,
        reference_service_1.ReferenceService])
], EventService);
exports.EventService = EventService;
//# sourceMappingURL=event.service.js.map