"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventController = void 0;
const filter_dto_1 = require("./dto/filter.dto");
const common_1 = require("@nestjs/common");
const shared_service_1 = require("../shared/shared.service");
const event_service_1 = require("./event.service");
const create_event_dto_1 = require("./dto/create-event.dto");
let EventController = class EventController {
    constructor(eventService, sservice) {
        this.eventService = eventService;
        this.sservice = sservice;
    }
    async createEvent(body, res) {
        try {
            let event = await this.eventService.createEvent(body, body.created_by, body.updated_by);
            return res.json(event);
        }
        catch (err) {
            const { code, response } = await this.sservice.processError(err, this.constructor.name);
            return res.status(code).json(response);
        }
    }
    async getEventList(query, res) {
        try {
            let data = await this.eventService.getEventList(query.skip, query.limit, query.search, query.category_id, query.language_id, query.city_id, query.start_date, query.end_date);
            return res.json(data);
        }
        catch (err) {
            const { code, response } = await this.sservice.processError(err, this.constructor.name);
            return res.status(code).json(response);
        }
    }
    async getEventDetail(id, res) {
        try {
            let data = await this.eventService.getEventDetail(id);
            return res.json(data);
        }
        catch (err) {
            const { code, response } = await this.sservice.processError(err, this.constructor.name);
            return res.status(code).json(response);
        }
    }
    async updateEvent(id, body, res) {
        try {
            let data = await this.eventService.updateEvent(id, body, body.created_by, body.updated_by);
            return res.json(data);
        }
        catch (err) {
            const { code, response } = await this.sservice.processError(err, this.constructor.name);
            return res.status(code).json(response);
        }
    }
    async updateEventStatus(id, status, res) {
        try {
            let data = await this.eventService.updateEventStatus(id, status);
            return res.json(data);
        }
        catch (err) {
            const { code, response } = await this.sservice.processError(err, this.constructor.name);
            return res.status(code).json(response);
        }
    }
};
__decorate([
    (0, common_1.Post)("create-event"),
    __param(0, (0, common_1.Body)(new common_1.ValidationPipe())),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_event_dto_1.CreateEventDTO, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "createEvent", null);
__decorate([
    (0, common_1.Post)("get-event-list"),
    __param(0, (0, common_1.Body)(new common_1.ValidationPipe())),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [filter_dto_1.FilterDTO, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "getEventList", null);
__decorate([
    (0, common_1.Get)("get-event-detail/:id"),
    __param(0, (0, common_1.Param)("id")),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "getEventDetail", null);
__decorate([
    (0, common_1.Patch)("update-event/:id"),
    __param(0, (0, common_1.Param)("id")),
    __param(1, (0, common_1.Body)(new common_1.ValidationPipe())),
    __param(2, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, create_event_dto_1.CreateEventDTO, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "updateEvent", null);
__decorate([
    (0, common_1.Patch)("update-event-status/:id/:status"),
    __param(0, (0, common_1.Param)("id")),
    __param(1, (0, common_1.Param)("status")),
    __param(2, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, Object]),
    __metadata("design:returntype", Promise)
], EventController.prototype, "updateEventStatus", null);
EventController = __decorate([
    (0, common_1.Controller)("event"),
    __metadata("design:paramtypes", [event_service_1.EventService,
        shared_service_1.SharedService])
], EventController);
exports.EventController = EventController;
//# sourceMappingURL=event.controller.js.map