import { FilterDTO } from './dto/filter.dto';
import { SharedService } from "src/shared/shared.service";
import { EventService } from "./event.service";
import { Response } from "express";
import { CreateEventDTO } from "./dto/create-event.dto";
export declare class EventController {
    private readonly eventService;
    private sservice;
    constructor(eventService: EventService, sservice: SharedService);
    createEvent(body: CreateEventDTO, res: Response): Promise<Response<any, Record<string, any>>>;
    getEventList(query: FilterDTO, res: Response): Promise<Response<any, Record<string, any>>>;
    getEventDetail(id: string, res: Response): Promise<Response<any, Record<string, any>>>;
    updateEvent(id: string, body: CreateEventDTO, res: Response): Promise<Response<any, Record<string, any>>>;
    updateEventStatus(id: string, status: string, res: Response): Promise<Response<any, Record<string, any>>>;
}
