import * as mongoose from "mongoose";
export interface ICategoryModel {
    category_id: string;
    category_name: string;
    category_key: string;
}
export interface IMedia {
    type?: string;
    media_id?: String;
    visibility?: string;
    media_url: string;
}
export interface ILanguageModel {
    language_id: string;
    language_code: string;
    language_name: string;
    language_native_name: string;
}
export interface IUserModel {
    id: string;
    user_name: string;
}
export interface ICityModel {
    city_name: string;
    city_id: string;
}
export interface IStateModel {
    state_name: string;
    state_id: string;
}
export interface ICountryModel {
    country_name: string;
    country_id: string;
}
export interface IAddressModel {
    address_first_line: string;
    address_second_line: string;
    city: ICityModel;
    state: IStateModel;
    country: ICountryModel;
    latitude: string;
    longitude: string;
}
export interface IEventModel extends mongoose.Document {
    name: string;
    type: string;
    sub_type: string;
    category: ICategoryModel;
    event_status: string;
    start_date: string;
    end_date?: string;
    description: string;
    tags: string[];
    link: string;
    additionalDetails: any;
    status: string;
    participantDetails: any[];
    documentStatus: string;
    media: IMedia[];
    language: ILanguageModel[];
    address: IAddressModel;
    created_by: IUserModel;
    updated_by: IUserModel;
}
export declare const UserSchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    [x: string]: any;
}>;
export declare const LanguageSchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    [x: string]: any;
}>;
export declare const MediaSchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    visibility: string;
    type?: string;
    media_id?: mongoose.Types.ObjectId;
    media_url?: string;
}>;
export declare const CountrySchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    [x: string]: any;
}>;
export declare const StateSchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    [x: string]: any;
}>;
export declare const CitySchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    [x: string]: any;
}>;
export declare const CategorySchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    [x: string]: any;
}>;
export declare const AddressSchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    [x: string]: any;
}>;
export declare const EventSchema: mongoose.Schema<any, mongoose.Model<any, any, any, any, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, {
    [x: string]: any;
}>;
