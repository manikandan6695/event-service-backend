"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventSchema = exports.AddressSchema = exports.CategorySchema = exports.CitySchema = exports.StateSchema = exports.CountrySchema = exports.MediaSchema = exports.LanguageSchema = exports.UserSchema = void 0;
const mongoose = require("mongoose");
exports.UserSchema = new mongoose.Schema({
    id: {
        type: String,
    },
    user_name: {
        type: String,
    },
});
exports.LanguageSchema = new mongoose.Schema({
    language_id: {
        type: String,
    },
    language_code: {
        type: String,
    },
    language_name: {
        type: String,
    },
    language_native_name: {
        type: String,
    },
});
exports.MediaSchema = new mongoose.Schema({
    type: {
        type: String,
        description: "Types of the media",
    },
    visibility: {
        type: String,
        description: "Privacy settings of email",
        default: "Public",
    },
    media_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "media",
    },
    media_url: {
        type: String,
    },
});
exports.CountrySchema = new mongoose.Schema({
    country_id: {
        type: String,
    },
    country_name: {
        type: String,
    },
});
exports.StateSchema = new mongoose.Schema({
    state_id: {
        type: String,
    },
    state_name: {
        type: String,
    },
});
exports.CitySchema = new mongoose.Schema({
    city_id: {
        type: String,
    },
    city_name: {
        type: String,
    },
});
exports.CategorySchema = new mongoose.Schema({
    category_id: {
        type: String,
    },
    category_name: {
        type: String,
    },
    category_key: {
        type: String,
    },
});
exports.AddressSchema = new mongoose.Schema({
    address_first_line: {
        type: String,
    },
    address_second_line: {
        type: String,
    },
    city: exports.CitySchema,
    state: exports.StateSchema,
    country: exports.CountrySchema,
    pincode: {
        type: String,
    },
    latitude: {
        type: String,
    },
    longitude: {
        type: String,
    },
});
exports.EventSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    type: {
        type: String,
    },
    sub_type: {
        type: String,
    },
    category: exports.CategorySchema,
    event_status: {
        type: String,
    },
    start_date: {
        type: Date,
    },
    end_date: {
        type: Date,
    },
    description: {
        type: String,
    },
    tags: [
        {
            type: String,
        },
    ],
    link: {
        type: String,
    },
    additionalDetails: {
        type: String,
    },
    status: {
        type: String,
        default: "Active",
    },
    documentStatus: {
        type: String,
        default: "Active",
    },
    media: [exports.MediaSchema],
    participantDetails: [
        {
            type: mongoose.Schema.Types.Mixed,
        },
    ],
    language: [exports.LanguageSchema],
    address: exports.AddressSchema,
    created_by: exports.UserSchema,
    updated_by: exports.UserSchema,
}, {
    collection: "event",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
});
//# sourceMappingURL=event.schema.js.map