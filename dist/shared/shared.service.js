"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypeMapping = exports.activity_icon = exports.app_variables = exports.app_report_key = exports.app_date_ranges = exports.SharedService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const app_exception_1 = require("./app-exception");
let SharedService = class SharedService {
    constructor(config) {
        this.config = config;
        this.defaultLanguage = "en";
        this.defaultExtension = "+91";
        this.schemas = {};
    }
    processError(err, context) {
        let code, response;
        if (err instanceof app_exception_1.AppException) {
            code = err.getCode();
            response = { code, message: err.getMessage() };
        }
        else {
            if (err.name == "MongoError") {
                if (err["code"] == 11000) {
                    let prop = "Value";
                    try {
                        prop = err.message.split(":")[2].replace(" dup key", "").trim();
                    }
                    catch (properr) {
                        console.log("cant get prop");
                    }
                    code = common_1.HttpStatus.NOT_ACCEPTABLE;
                    response = { code, message: `${prop} provided already exist` };
                }
                else {
                    code = common_1.HttpStatus.INTERNAL_SERVER_ERROR;
                    response = { code, message: err.message };
                }
            }
            else {
                code = common_1.HttpStatus.INTERNAL_SERVER_ERROR;
                response = { code, message: err.message };
            }
        }
        console.log("going to process err", err.stack);
        return { code, response };
    }
};
SharedService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService])
], SharedService);
exports.SharedService = SharedService;
var app_date_ranges;
(function (app_date_ranges) {
    app_date_ranges["all"] = "all";
    app_date_ranges["this_month"] = "this_month";
    app_date_ranges["this_week"] = "this_week";
    app_date_ranges["yesterday"] = "yesterday";
    app_date_ranges["today"] = "today";
    app_date_ranges["last_seven_days"] = "last_seven_days";
    app_date_ranges["last_three_months"] = "last_three_months";
    app_date_ranges["last_six_months"] = "last_six_months";
    app_date_ranges["last_twelve_months"] = "last_twelve_months";
    app_date_ranges["this_year"] = "this_year";
    app_date_ranges["this_financial_year"] = "this_financial_year";
    app_date_ranges["last_financial_year"] = "last_financial_year";
    app_date_ranges["last_year"] = "last_year";
    app_date_ranges["custom"] = "custom";
})(app_date_ranges = exports.app_date_ranges || (exports.app_date_ranges = {}));
var app_report_key;
(function (app_report_key) {
    app_report_key["sales_by_customer"] = "sales-by-customer";
    app_report_key["sales_by_branch"] = "sales-by-branch";
    app_report_key["sales_by_item"] = "sales-by-item";
    app_report_key["invoice_report"] = "invoice-report";
    app_report_key["payment_received_report"] = "payment-received-report";
    app_report_key["customer_balance"] = "customer-balance";
    app_report_key["inventory_summary_report"] = "inventory-summary-report";
})(app_report_key = exports.app_report_key || (exports.app_report_key = {}));
exports.app_variables = {
    default_language_id: "6091525efccd22ae080d9107",
    default_db_date_format_id: "6177bd95c8f14ed85c69aba8",
    default_db_date_format: "%d/%m/%Y",
    default_decimal_digits: 2,
    default_org_fcode: "organization",
    default_role_fcode: "ROLE",
    default_branch_fcode: "BRCH",
    default_warehouse_fcode: "WREHS",
    default_customer_fcode: "CUST",
    default_ecom_customer_fcode: "CUSTB2C",
    default_sales_person_fcode: "SLP",
    default_vendor_fcode: "VEND",
    default_user_fcode: "USR",
    support_ticket_fcode: "SPT",
    support_ticket_conv_fcode: "STC",
    item_fcode: "ITM",
    inquiry_fcode: "INQ",
    estimation_fcode: "EST",
    invoice_fcode: "INVO",
    sale_order_fcode: "SO",
    payment_received_fcode: "PMTR",
    package_fcode: "PACK",
    shipment_fcode: "SHIP",
    channel_digital_catalogue: "digital_catalogue",
    channel_online_store: "online_store",
    channel_wholesale: "wholesale",
    channel_retail: "retail",
    default_item_category_type: "items",
    default_subscription_fcode: "subscription",
    default_transfer_order_fcode: "transfer-order",
    default_deals_fcode: "deals",
    default_inventory_adjustment_fcode: "inventory-adjustment",
    default_calls_fcode: "calls",
    default_opening_balance_fcode: "opening-balance",
    default_opening_balance_detail_fcode: "opening-balance-detail",
    default_classification_fcode: "classification",
    default_merchant_fcode: "merchant",
    default_expenses_fcode: "expenses",
    default_expense_report_fcode: "expense-report",
    default_expense_tag_fcode: "expense-tag",
    default_general_ledger_fcode: "general-ledger",
    default_reason_fcode: "reason",
    default_purchase_order_fcode: "purchase-order",
    default_credit_note_fcode: "credit-notes",
    default_bills_fcode: "bills",
    default_payment_made_fcode: "PMTM",
    default_tds_fcode: "tds",
    default_tcs_fcode: "tcs",
    default_account_transaction_fcode: "account-transaction",
    default_refund_fcode: "refund",
    default_online_store_fcode: "online-store",
    default_collection_fcode: "collection",
    default_sort_fcode: "sort",
    default_discount_fcode: "discount",
    default_legals_fcode: "legal",
    default_reviews_fcode: "reviews",
    default_review_response_fcode: "review-response",
    default_nav_menu_fcode: "nav-menu",
    default_nav_menu_items_fcode: "nav-menu-items",
    default_sale_return_fcode: "sale-return",
    default_receive_fcode: "return-receipt",
    default_shipping_zone_fcode: "shipping-zone",
    default_shipping_region_fcode: "shipping-region",
    default_shipping_rate_fcode: "shipping-rate",
    default_cart_fcode: "cart",
    default_ecom_offline_payments_fcode: "ecom-offline-payments",
    default_app_credential_fcode: "app-credential",
    default_sn_org_profile_fcode: "sn-org-profile",
    default_feature_request_fcode: "feature-request",
    default_sa_feature_request_fcode: "sa-feature-request",
};
var activity_icon;
(function (activity_icon) {
    activity_icon["status_change"] = "feather icon-refresh-ccw";
    activity_icon["assigned_to_user"] = "feather icon-user-check";
    activity_icon["file_text"] = "feather icon-file-text";
})(activity_icon = exports.activity_icon || (exports.activity_icon = {}));
var TypeMapping;
(function (TypeMapping) {
    TypeMapping["string"] = "isString";
    TypeMapping["objectid"] = "isMongoId";
    TypeMapping["boolean"] = "isBooleanString";
    TypeMapping["number"] = "isNumber";
})(TypeMapping = exports.TypeMapping || (exports.TypeMapping = {}));
//# sourceMappingURL=shared.service.js.map