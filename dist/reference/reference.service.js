"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReferenceService = void 0;
const common_1 = require("@nestjs/common");
const axios_1 = require("@nestjs/axios");
const shared_service_1 = require("../shared/shared.service");
const config_1 = require("@nestjs/config");
let ReferenceService = class ReferenceService {
    constructor(http_service, config_service, shared_service) {
        this.http_service = http_service;
        this.config_service = config_service;
        this.shared_service = shared_service;
    }
    async getUserById(user_id) {
        try {
            let body = {
                user_id: user_id,
            };
            console.log("body is", body);
            console.log("url is", this.config_service.get("CASTTREE_BASE_URL"));
            let data = await this.http_service
                .post(`${this.config_service.get("CASTTREE_BASE_URL")}/user/get-user-detail`, body)
                .toPromise();
            console.log("data is", data);
            return data.data;
        }
        catch (err) {
            throw err;
        }
    }
    async updateUserDetail(user_id, phone_number, email) {
        try {
            let body = {
                user_id: user_id,
                phone_number: phone_number,
                email: email,
            };
            let data = await this.http_service
                .post(`${this.config_service.get("CASTTREE_BASE_URL")}/user/update-user-detail`, body)
                .toPromise();
            return data.data;
        }
        catch (err) {
            throw err;
        }
    }
};
ReferenceService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [axios_1.HttpService,
        config_1.ConfigService,
        shared_service_1.SharedService])
], ReferenceService);
exports.ReferenceService = ReferenceService;
//# sourceMappingURL=reference.service.js.map