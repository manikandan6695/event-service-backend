import { HttpService } from "@nestjs/axios";
import { SharedService } from "src/shared/shared.service";
import { ConfigService } from "@nestjs/config";
export declare class ReferenceService {
    private http_service;
    private config_service;
    private shared_service;
    constructor(http_service: HttpService, config_service: ConfigService, shared_service: SharedService);
    getUserById(user_id: string): Promise<any>;
    updateUserDetail(user_id: string, phone_number: string, email: string): Promise<any>;
}
