"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const bodyParser = require("body-parser");
require("dotenv/config");
const helmet = require("helmet");
const app_module_1 = require("./app.module");
const port = process.env.SERVER_PORT || 3100;
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.use(helmet());
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Headers", "application-key");
        next();
    });
    app.use(bodyParser.json({ limit: "50mb" }));
    app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
    app.setGlobalPrefix(process.env.API_PREFIX);
    app.enableCors({
        origin: "*",
    });
    await app.listen(port);
    console.log(`${new Date()}====>App started in port ${port}<====`);
}
bootstrap();
//# sourceMappingURL=main.js.map